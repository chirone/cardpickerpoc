//
//  CardPickerView.swift
//  CardPicker
//
//  Created by Hayden Young on 31/03/2019.
//  Copyright © 2019 Seekers. All rights reserved.
//

import UIKit

typealias CardPickerCellType = UICollectionViewCell & CardPickerCellProtocol

protocol CardViewModelProtocol {
	var name: String? { get }
	var details: String? { get }
	var image: UIImage? { get }
}

protocol CardPickerRepositoryProtocol {
	var cardViewModels: [CardViewModelProtocol] { get }
}

protocol CardPickerCellProtocol {
	var viewModel: CardViewModelProtocol? { get set }
}

class CardPickerView: UIView {

	@IBOutlet weak var collectionView: UICollectionView!
	// set repo - get models - alter datasource
	var repository: CardPickerRepositoryProtocol? {
		didSet {
			// - get view models and re-bind
		}
	}
	
	var cardDidSelectItem: ((_ cardViewModel: CardViewModelProtocol?) -> Void)? // the CardViewModelProtocol shouldn't be optional, it's just here because I have no implementation that conforms to the protocol
	
	class func cardPickerViewWith(cellClass: CardPickerCellType.Type = CardPickerBasicCell.self) -> CardPickerView {
		var cardPickerView: CardPickerView!
		if let views = Bundle.main.loadNibNamed("CardPickerView", owner: nil),
			let firstView = views.first as? CardPickerView {
			cardPickerView = firstView
		} else {
			cardPickerView = CardPickerView()
		}
		
		cardPickerView.collectionView.register(cellClass, forCellWithReuseIdentifier: "Cell")
		return cardPickerView
	}
	
}

extension CardPickerView: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let cardDidSelectItem = self.cardDidSelectItem else {
			return
		}
		cardDidSelectItem(nil)
	}
}
