//
//  CardPickerBasicCell.swift
//  CardPicker
//
//  Created by Hayden Young on 31/03/2019.
//  Copyright © 2019 Seekers. All rights reserved.
//

import UIKit

class CardPickerBasicCell: CardPickerCellType {
	
	@IBOutlet private(set) var titleLabel: UILabel!
	@IBOutlet private(set) var detailsLabel: UILabel!
	@IBOutlet private(set) var imageView: UIImageView!
	@IBOutlet private(set) var userImageView: UIImageView! // this will probably need to change from an image view to something else because there's a blue dot which I assume means the user who posted the guide is online.
	
	var viewModel: CardViewModelProtocol? {
		didSet {
			setupBindings()
		}
	}
	
	private func setupBindings() {
		
	}
}
